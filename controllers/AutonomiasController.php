<?php

namespace app\controllers;

use app\models\autonomias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AutonomiasController implements the CRUD actions for autonomias model.
 */
class AutonomiasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all autonomias models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => autonomias::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idAutonomia' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single autonomias model.
     * @param int $idAutonomia Id Autonomia
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idAutonomia)
    {
        return $this->render('view', [
            'model' => $this->findModel($idAutonomia),
        ]);
    }

    /**
     * Creates a new autonomias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new autonomias();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idAutonomia' => $model->idAutonomia]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing autonomias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idAutonomia Id Autonomia
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idAutonomia)
    {
        $model = $this->findModel($idAutonomia);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAutonomia' => $model->idAutonomia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing autonomias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idAutonomia Id Autonomia
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idAutonomia)
    {
        $this->findModel($idAutonomia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the autonomias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idAutonomia Id Autonomia
     * @return autonomias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idAutonomia)
    {
        if (($model = autonomias::findOne(['idAutonomia' => $idAutonomia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
