<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\ContactForm;
use app\models\Provincias;
use app\models\Spots;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionMapapaises() {
        return $this->render('mapa-paises');
    }

    public function actionMapaprovinciasespana() {
        return $this->render('mapa-provincias-españa');
    }

    public function actionSpots() {
        return $this->render('spots');
    }

    public function actionConsultapais($nombrepais) {
        $eprovincias = new ActiveDataProvider([
            'query' => Provincias::find()->select('COUNT(idProvincia)nprovincias')->distinct()->where("idprovincia IN (SELECT idProvincia FROM provincias WHERE idautonomia IN(SELECT idAutonomia FROM autonomias  WHERE idpais=(SELECT idPais FROM paises  WHERE nombrePais='$nombrepais')))"),
        ]);
        $numerospotsprovincia = yii:: $app->db->createCommand("SELECT nombreProvincia,COUNT(*)nspots  FROM spots INNER JOIN (SELECT idMunicipio, idprovincia FROM municipios WHERE idprovincia IN(SELECT idProvincia FROM provincias WHERE idautonomia 
            IN(SELECT idAutonomia FROM autonomias  WHERE idpais=(SELECT idPais FROM paises  WHERE nombrePais='$nombrepais'))))c1 USING(idMunicipio) INNER JOIN (
  SELECT idProvincia, nombreProvincia FROM provincias WHERE idautonomia IN(SELECT idAutonomia FROM autonomias  WHERE idpais=(SELECT idPais FROM paises  WHERE nombrePais='$nombrepais'))
)c2 USING(idProvincia) GROUP BY c2.idProvincia")->queryAll();
        return $this->render('mapa-provincias-españa', [
                    "numerospotsprovincia" => $numerospotsprovincia,
                    "nombrepais" => $nombrepais,
                    "eprovincias" => $eprovincias,
        ]);
    }
    
     public function actionConsultaprovincia($nombreprovincia) {
        $spots = new SqlDataProvider([
            'sql'=>"select fecha,descripcion,posiblesPlanes,latitud,altitud from spots where idmunicipio IN (SELECT idMunicipio FROM municipios WHERE idprovincia IN(select idProvincia from provincias where nombreProvincia='$nombreprovincia'))",
        ]);
        
        return $this->render('spots', [
                   "spots"=>$spots,
            "nombreprovincia"=>$nombreprovincia,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
