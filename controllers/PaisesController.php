<?php

namespace app\controllers;

use app\models\paises;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaisesController implements the CRUD actions for paises model.
 */
class PaisesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all paises models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => paises::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idPais' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single paises model.
     * @param int $idPais Id Pais
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idPais)
    {
        return $this->render('view', [
            'model' => $this->findModel($idPais),
        ]);
    }

    /**
     * Creates a new paises model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new paises();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idPais' => $model->idPais]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing paises model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idPais Id Pais
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idPais)
    {
        $model = $this->findModel($idPais);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idPais' => $model->idPais]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing paises model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idPais Id Pais
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idPais)
    {
        $this->findModel($idPais)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the paises model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idPais Id Pais
     * @return paises the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idPais)
    {
        if (($model = paises::findOne(['idPais' => $idPais])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
