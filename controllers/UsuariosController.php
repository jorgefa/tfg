<?php

namespace app\controllers;

use app\models\usuarios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuariosController implements the CRUD actions for usuarios model.
 */
class UsuariosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all usuarios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => usuarios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dNi' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single usuarios model.
     * @param string $dNi D Ni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dNi)
    {
        return $this->render('view', [
            'model' => $this->findModel($dNi),
        ]);
    }

    /**
     * Creates a new usuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new usuarios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dNi' => $model->dNi]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing usuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dNi D Ni
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dNi)
    {
        $model = $this->findModel($dNi);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dNi' => $model->dNi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing usuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dNi D Ni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dNi)
    {
        $this->findModel($dNi)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dNi D Ni
     * @return usuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dNi)
    {
        if (($model = usuarios::findOne(['dNi' => $dNi])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
