<?php

namespace app\controllers;

use app\models\municipios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MunicipiosController implements the CRUD actions for municipios model.
 */
class MunicipiosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all municipios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => municipios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idMunicipio' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single municipios model.
     * @param int $idMunicipio Id Municipio
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idMunicipio)
    {
        return $this->render('view', [
            'model' => $this->findModel($idMunicipio),
        ]);
    }

    /**
     * Creates a new municipios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new municipios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idMunicipio' => $model->idMunicipio]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing municipios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idMunicipio Id Municipio
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idMunicipio)
    {
        $model = $this->findModel($idMunicipio);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idMunicipio' => $model->idMunicipio]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing municipios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idMunicipio Id Municipio
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idMunicipio)
    {
        $this->findModel($idMunicipio)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the municipios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idMunicipio Id Municipio
     * @return municipios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idMunicipio)
    {
        if (($model = municipios::findOne(['idMunicipio' => $idMunicipio])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
