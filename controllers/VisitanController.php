<?php

namespace app\controllers;

use app\models\visitan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VisitanController implements the CRUD actions for visitan model.
 */
class VisitanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all visitan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => visitan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idVisita' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single visitan model.
     * @param int $idVisita Id Visita
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idVisita)
    {
        return $this->render('view', [
            'model' => $this->findModel($idVisita),
        ]);
    }

    /**
     * Creates a new visitan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new visitan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idVisita' => $model->idVisita]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing visitan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idVisita Id Visita
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idVisita)
    {
        $model = $this->findModel($idVisita);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idVisita' => $model->idVisita]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing visitan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idVisita Id Visita
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idVisita)
    {
        $this->findModel($idVisita)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the visitan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idVisita Id Visita
     * @return visitan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idVisita)
    {
        if (($model = visitan::findOne(['idVisita' => $idVisita])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
