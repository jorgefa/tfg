<?php

namespace app\controllers;

use app\models\spots;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpotsController implements the CRUD actions for spots model.
 */
class SpotsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all spots models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => spots::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idSpot' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single spots model.
     * @param int $idSpot Id Spot
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idSpot)
    {
        return $this->render('view', [
            'model' => $this->findModel($idSpot),
        ]);
    }

    /**
     * Creates a new spots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new spots();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idSpot' => $model->idSpot]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing spots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idSpot Id Spot
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idSpot)
    {
        $model = $this->findModel($idSpot);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idSpot' => $model->idSpot]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing spots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idSpot Id Spot
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idSpot)
    {
        $this->findModel($idSpot)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the spots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idSpot Id Spot
     * @return spots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idSpot)
    {
        if (($model = spots::findOne(['idSpot' => $idSpot])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
