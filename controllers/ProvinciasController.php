<?php

namespace app\controllers;

use app\models\provincias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProvinciasController implements the CRUD actions for provincias model.
 */
class ProvinciasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all provincias models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => provincias::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idProvincia' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single provincias model.
     * @param int $idProvincia Id Provincia
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idProvincia)
    {
        return $this->render('view', [
            'model' => $this->findModel($idProvincia),
        ]);
    }

    /**
     * Creates a new provincias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new provincias();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idProvincia' => $model->idProvincia]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing provincias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idProvincia Id Provincia
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idProvincia)
    {
        $model = $this->findModel($idProvincia);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idProvincia' => $model->idProvincia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing provincias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idProvincia Id Provincia
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idProvincia)
    {
        $this->findModel($idProvincia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the provincias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idProvincia Id Provincia
     * @return provincias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idProvincia)
    {
        if (($model = provincias::findOne(['idProvincia' => $idProvincia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
