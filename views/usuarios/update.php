<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\usuarios $model */

$this->title = 'Update Usuarios: ' . $model->dNi;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dNi, 'url' => ['view', 'dNi' => $model->dNi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
