<?php

use app\models\spots;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Spots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spots-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Spots', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idSpot',
            'fecha',
            'descripcion',
            'ubicacion',
            'posiblesPlanes',
            //'idmunicipio',
            //'dni',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, spots $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idSpot' => $model->idSpot]);
                 }
            ],
        ],
    ]); ?>


</div>
