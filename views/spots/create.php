<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\spots $model */

$this->title = 'Create Spots';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spots-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
