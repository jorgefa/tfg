<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\spots $model */

$this->title = $model->idSpot;
$this->params['breadcrumbs'][] = ['label' => 'Spots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spots-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idSpot' => $model->idSpot], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idSpot' => $model->idSpot], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSpot',
            'fecha',
            'descripcion',
            'ubicacion',
            'posiblesPlanes',
            'idmunicipio',
            'dni',
        ],
    ]) ?>

</div>
