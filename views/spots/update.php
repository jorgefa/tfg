<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\spots $model */

$this->title = 'Update Spots: ' . $model->idSpot;
$this->params['breadcrumbs'][] = ['label' => 'Spots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idSpot, 'url' => ['view', 'idSpot' => $model->idSpot]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="spots-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
