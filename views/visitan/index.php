<?php

use app\models\visitan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Visitans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Visitan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idVisita',
            'dni',
            'idspot',
            'puntuacion',
            'comentarioU:ntext',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, visitan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idVisita' => $model->idVisita]);
                 }
            ],
        ],
    ]); ?>


</div>
