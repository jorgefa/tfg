<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\visitan $model */

$this->title = 'Create Visitan';
$this->params['breadcrumbs'][] = ['label' => 'Visitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
