<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\visitan $model */

$this->title = 'Update Visitan: ' . $model->idVisita;
$this->params['breadcrumbs'][] = ['label' => 'Visitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idVisita, 'url' => ['view', 'idVisita' => $model->idVisita]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="visitan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
