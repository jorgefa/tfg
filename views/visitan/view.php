<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\visitan $model */

$this->title = $model->idVisita;
$this->params['breadcrumbs'][] = ['label' => 'Visitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="visitan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idVisita' => $model->idVisita], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idVisita' => $model->idVisita], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idVisita',
            'dni',
            'idspot',
            'puntuacion',
            'comentarioU:ntext',
        ],
    ]) ?>

</div>
