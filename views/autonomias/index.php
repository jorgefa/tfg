<?php

use app\models\autonomias;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Autonomias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autonomias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Autonomias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAutonomia',
            'nombreAutonomia',
            'idpais',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, autonomias $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idAutonomia' => $model->idAutonomia]);
                 }
            ],
        ],
    ]); ?>


</div>
