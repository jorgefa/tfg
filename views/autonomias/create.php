<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\autonomias $model */

$this->title = 'Create Autonomias';
$this->params['breadcrumbs'][] = ['label' => 'Autonomias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autonomias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
