<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\autonomias $model */

$this->title = $model->idAutonomia;
$this->params['breadcrumbs'][] = ['label' => 'Autonomias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="autonomias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idAutonomia' => $model->idAutonomia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idAutonomia' => $model->idAutonomia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idAutonomia',
            'nombreAutonomia',
            'idpais',
        ],
    ]) ?>

</div>
