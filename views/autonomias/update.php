<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\autonomias $model */

$this->title = 'Update Autonomias: ' . $model->idAutonomia;
$this->params['breadcrumbs'][] = ['label' => 'Autonomias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAutonomia, 'url' => ['view', 'idAutonomia' => $model->idAutonomia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="autonomias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
