<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

use yii\helpers\Html;
?>
<div class="">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido a Spots!</h1>
    </div>

    <div class="body-content-paises">

        <div class="row">
            <div class="col-lg-2">
                <h2>Ir al mapa</h2>

                <p>Desde esta pestaña accederemos a un mapa, a la iz tendremos el numero de spots por paises que tenemos y en cuales se encuentran,
                    al llegar a la provincia deseada nos mostrara todos los spots que tiene esta agrupados por los municipios donde se encuentran,
                    pudiendo acceder a un mapa de la ubicacion proporcionado por las coordenadas.</p>
                <?= Html::a('Mapa 2', ['site/mapaprovinciasespana'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-10 medidasmapas">
                <?= Html::img('@web/img/mapa-paises.jpg', ['alt' => 'Mapa Paises']) ?>
                <?= Html::a($text = null, ['site/consultapais','nombrepais' => 'España'], ['title'=>'España','class' => 'botonmapa btespaña']) ?>
                
            </div>

        </div>

    </div>
</div>