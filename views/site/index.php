<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido a Spots!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Estadisticas</h2>

                <p>Aqui podremos ver la puntuacion medias de los spots por paises, autonomias, provincias y municipios,
                    tambien podremos ver quien es el que mas spots tiene, cual es el mejor puntuado, cual es el mas visitado...</p>

                <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Ir al mapa</h2>

                <p>Desde esta pestaña accederemos a un mapa, a la iz tendremos el numero de spots por paises que tenemos y en cuales se encuentran,
                    al llegar a la provincia deseada nos mostrara todos los spots que tiene esta agrupados por los municipios donde se encuentran,
                    pudiendo acceder a un mapa de la ubicacion proporcionado por las coordenadas.</p>

                <?= Html::a('Mapa', ['site/mapapaises'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-4">
                <h2>Añadir un Spot</h2>

                <p>De alguna manera los usuarios podran crear spots introduciendo su dni y el municipio donde se encuentra.</p>

                <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
