<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

use yii\widgets\ListView;
use yii\helpers\Html;
?>
<div class="">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido a <?= $nombrepais ?>!</h1>
    </div>

    <div class="body-content-paises">

        <div class="row">
            <div class="col-lg-2">
                <h2>Provincia/Spots</h2>
                <?php
                echo ListView::widget([
                    'dataProvider' => $eprovincias,
                    'itemView' => '_tarjetaprovincias',
                    'layout' => " \n {items} \n\n{pager}",
                    'viewParams' => [
                        "numerospotsprovincia" => $numerospotsprovincia,
                        "nombrepais" => $nombrepais,
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-10 medidasmapas">
                <?= Html::img('@web/img/mapa-provincias-españa.jpg', ['alt' => 'Mapa Paises']) ?>
                <?= Html::a($text = null, ['site/consultaprovincia', 'nombreprovincia' => 'Cantabria'], ['title' => 'Cantabria', 'class' => 'botonmapa btcantabria']) ?>
                
            </div>

        </div>

    </div>
</div>