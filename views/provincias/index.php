<?php

use app\models\provincias;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Provincias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provincias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Provincias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProvincia',
            'nombreProvincia',
            'idautonomia',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, provincias $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idProvincia' => $model->idProvincia]);
                 }
            ],
        ],
    ]); ?>


</div>
