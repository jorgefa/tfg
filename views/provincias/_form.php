<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\provincias $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="provincias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreProvincia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idautonomia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
