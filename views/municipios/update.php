<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\municipios $model */

$this->title = 'Update Municipios: ' . $model->idMunicipio;
$this->params['breadcrumbs'][] = ['label' => 'Municipios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idMunicipio, 'url' => ['view', 'idMunicipio' => $model->idMunicipio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="municipios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
