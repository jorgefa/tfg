<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\municipios $model */

$this->title = $model->idMunicipio;
$this->params['breadcrumbs'][] = ['label' => 'Municipios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="municipios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idMunicipio' => $model->idMunicipio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idMunicipio' => $model->idMunicipio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idMunicipio',
            'nombreMunicipio',
            'idprovincia',
        ],
    ]) ?>

</div>
