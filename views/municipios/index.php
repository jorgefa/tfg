<?php

use app\models\municipios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Municipios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="municipios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Municipios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idMunicipio',
            'nombreMunicipio',
            'idprovincia',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, municipios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idMunicipio' => $model->idMunicipio]);
                 }
            ],
        ],
    ]); ?>


</div>
