<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\paises $model */

$this->title = 'Update Paises: ' . $model->idPais;
$this->params['breadcrumbs'][] = ['label' => 'Paises', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPais, 'url' => ['view', 'idPais' => $model->idPais]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paises-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
