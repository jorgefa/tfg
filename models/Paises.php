<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property int $idPais
 * @property string|null $nombrePais
 *
 * @property Autonomias[] $autonomias
 */
class Paises extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombrePais'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPais' => 'Id Pais',
            'nombrePais' => 'Nombre Pais',
        ];
    }

    /**
     * Gets query for [[Autonomias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutonomias()
    {
        return $this->hasMany(Autonomias::class, ['idpais' => 'idPais']);
    }
}
