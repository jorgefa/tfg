<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitan".
 *
 * @property int $idVisita
 * @property string|null $dni
 * @property int|null $idspot
 * @property float|null $puntuacion
 * @property string|null $comentarioU
 *
 * @property Usuarios $dni0
 * @property Spots $idspot0
 */
class Visitan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idspot'], 'integer'],
            [['puntuacion'], 'number'],
            [['comentarioU'], 'string'],
            [['dni'], 'string', 'max' => 10],
            [['idspot'], 'exist', 'skipOnError' => true, 'targetClass' => Spots::class, 'targetAttribute' => ['idspot' => 'idSpot']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['dni' => 'dNi']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idVisita' => 'Id Visita',
            'dni' => 'Dni',
            'idspot' => 'Idspot',
            'puntuacion' => 'Puntuacion',
            'comentarioU' => 'Comentario U',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Usuarios::class, ['dNi' => 'dni']);
    }

    /**
     * Gets query for [[Idspot0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdspot0()
    {
        return $this->hasOne(Spots::class, ['idSpot' => 'idspot']);
    }
}
