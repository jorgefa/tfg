<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spots".
 *
 * @property int $idSpot
 * @property string|null $fecha
 * @property string|null $descripcion
 * @property string|null $posiblesPlanes
 * @property int|null $idmunicipio
 * @property string|null $dni
 * @property float|null $latitud
 * @property float|null $altitud
 *
 * @property Usuarios $dni0
 * @property Fotos[] $fotos
 * @property Municipios $idmunicipio0
 * @property Visitan[] $visitans
 */
class Spots extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spots';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['idmunicipio'], 'integer'],
            [['latitud', 'altitud'], 'number'],
            [['descripcion'], 'string', 'max' => 2000],
            [['posiblesPlanes'], 'string', 'max' => 1000],
            [['dni'], 'string', 'max' => 10],
            [['idmunicipio'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::class, 'targetAttribute' => ['idmunicipio' => 'idMunicipio']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['dni' => 'dNi']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idSpot' => 'Id Spot',
            'fecha' => 'Fecha',
            'descripcion' => 'Descripcion',
            'posiblesPlanes' => 'Posibles Planes',
            'idmunicipio' => 'Idmunicipio',
            'dni' => 'Dni',
            'latitud' => 'Latitud',
            'altitud' => 'Altitud',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Usuarios::class, ['dNi' => 'dni']);
    }

    /**
     * Gets query for [[Fotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::class, ['idspot' => 'idSpot']);
    }

    /**
     * Gets query for [[Idmunicipio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmunicipio0()
    {
        return $this->hasOne(Municipios::class, ['idMunicipio' => 'idmunicipio']);
    }

    /**
     * Gets query for [[Visitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitans()
    {
        return $this->hasMany(Visitan::class, ['idspot' => 'idSpot']);
    }
}
