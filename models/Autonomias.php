<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autonomias".
 *
 * @property int $idAutonomia
 * @property string|null $nombreAutonomia
 * @property int|null $idpais
 *
 * @property Paises $idpais0
 * @property Provincias[] $provincias
 */
class Autonomias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autonomias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpais'], 'integer'],
            [['nombreAutonomia'], 'string', 'max' => 50],
            [['idpais'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['idpais' => 'idPais']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAutonomia' => 'Id Autonomia',
            'nombreAutonomia' => 'Nombre Autonomia',
            'idpais' => 'Idpais',
        ];
    }

    /**
     * Gets query for [[Idpais0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpais0()
    {
        return $this->hasOne(Paises::class, ['idPais' => 'idpais']);
    }

    /**
     * Gets query for [[Provincias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincias::class, ['idautonomia' => 'idAutonomia']);
    }
}
