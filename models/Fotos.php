<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $idFoto
 * @property int|null $idspot
 * @property string|null $foto
 *
 * @property Spots $idspot0
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idspot'], 'integer'],
            [['foto'], 'string'],
            [['idspot'], 'exist', 'skipOnError' => true, 'targetClass' => Spots::class, 'targetAttribute' => ['idspot' => 'idSpot']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idFoto' => 'Id Foto',
            'idspot' => 'Idspot',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Idspot0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdspot0()
    {
        return $this->hasOne(Spots::class, ['idSpot' => 'idspot']);
    }
}
