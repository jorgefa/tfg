<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property int $idProvincia
 * @property string|null $nombreProvincia
 * @property int|null $idautonomia
 *
 * @property Autonomias $idautonomia0
 * @property Municipios[] $municipios
 */
class Provincias extends \yii\db\ActiveRecord
{
    public $nprovincias;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idautonomia'], 'integer'],
            [['nombreProvincia'], 'string', 'max' => 50],
            [['idautonomia'], 'exist', 'skipOnError' => true, 'targetClass' => Autonomias::class, 'targetAttribute' => ['idautonomia' => 'idAutonomia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProvincia' => 'Id Provincia',
            'nombreProvincia' => 'Nombre Provincia',
            'idautonomia' => 'Idautonomia',
        ];
    }

    /**
     * Gets query for [[Idautonomia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdautonomia0()
    {
        return $this->hasOne(Autonomias::class, ['idAutonomia' => 'idautonomia']);
    }

    /**
     * Gets query for [[Municipios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipios()
    {
        return $this->hasMany(Municipios::class, ['idprovincia' => 'idProvincia']);
    }
}
