<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "municipios".
 *
 * @property int $idMunicipio
 * @property string|null $nombreMunicipio
 * @property int|null $idprovincia
 *
 * @property Provincias $idprovincia0
 * @property Spots[] $spots
 */
class Municipios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'municipios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idprovincia'], 'integer'],
            [['nombreMunicipio'], 'string', 'max' => 50],
            [['idprovincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::class, 'targetAttribute' => ['idprovincia' => 'idProvincia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idMunicipio' => 'Id Municipio',
            'nombreMunicipio' => 'Nombre Municipio',
            'idprovincia' => 'Idprovincia',
        ];
    }

    /**
     * Gets query for [[Idprovincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdprovincia0()
    {
        return $this->hasOne(Provincias::class, ['idProvincia' => 'idprovincia']);
    }

    /**
     * Gets query for [[Spots]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpots()
    {
        return $this->hasMany(Spots::class, ['idmunicipio' => 'idMunicipio']);
    }
}
