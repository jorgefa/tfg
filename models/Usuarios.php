<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property string $dNi
 * @property string|null $nombre
 * @property string|null $contraseña
 *
 * @property Spots[] $spots
 * @property Visitan[] $visitans
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dNi'], 'required'],
            [['dNi'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 25],
            [['contraseña'], 'string', 'max' => 50],
            [['dNi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dNi' => 'D Ni',
            'nombre' => 'Nombre',
            'contraseña' => 'Contraseña',
        ];
    }

    /**
     * Gets query for [[Spots]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpots()
    {
        return $this->hasMany(Spots::class, ['dni' => 'dNi']);
    }

    /**
     * Gets query for [[Visitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitans()
    {
        return $this->hasMany(Visitan::class, ['dni' => 'dNi']);
    }
}
